<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;

/**
 * BookQuery represents the model behind the search form about `app\models\Book`.
 */
class BookQuery extends Book
{

    public $date1;
    public $date2;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date' => 'Дата',
            'date1' => 'Дата',
            'date2' => '',
            'author_id' => 'Автор',
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'author_id'], 'integer'],
            [['name', 'preview', 'date', 'date1', 'date2', 'author'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        $query->joinWith(['author']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 10]
        ]);

        $dataProvider->sort->attributes['author.firstname'] = [
            'asc' => ['{{%author}}.firstname' => SORT_ASC],
            'desc' => ['{{%author}}.firstname' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        if($this->date1 and $this->date2)
            $query->andWhere(['between', '{{%book}}.date', $this->date1.' 00:00:00', $this->date2.' 23:59:59']);
        elseif($this->date1 and !$this->date2)
            $query->andWhere(['>=', '{{%book}}.date', $this->date1]);
        elseif(!$this->date1 and $this->date2)
            $query->andWhere(['<=', '{{%book}}.date', $this->date2]);

        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
        ]);


        $query->andFilterWhere(['like', 'name', $this->name]);

        $dataProvider->query = $query;

        return $dataProvider;
    }
}
