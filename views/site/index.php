<?php
/* @var $this yii\web\View */
$this->title = 'DemoApp';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Привет!</h1>

        <p class="lead">Это тестовое задание для RGK.</p>

        <p><a class="btn btn-lg btn-success" href="<?=Yii::$app->urlManager->createUrl('book/index')?>">Книги</a></p>
    </div>
</div>
