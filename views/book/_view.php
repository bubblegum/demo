<?
/** @var $model \app\models\Book */
if(!$model->preview) {
    $preview = '';
} else {
    $_preview = '/upload/image/' . $model->preview;
    $img = \yii\helpers\Html::img($_preview, ['width' => 200]);

    $preview = \yii\helpers\Html::a($img, '#', [
        'class'      => 'showModalButton',
        'data-image' => $_preview,
    ]);
}
?>
<?= \yii\widgets\DetailView::widget([
    'model'      => $model,
    'attributes' => [
        'id',
        'name',
        [
            'attribute' => 'created_at',
            'value' => date("Y-m-d H:i:s", $model->created_at)
        ],
        [
            'attribute' => 'updated_at',
            'value' => date("Y-m-d H:i:s", $model->updated_at)
        ],
        [
            'attribute' => 'preview',
            'format' => 'raw',
            'value' => $preview
        ],
        'date',
        [
            'attribute' => 'author_id',
            'value' => $model->author->firstname.' '.$model->author->lastname
        ],
    ],
]) ?>