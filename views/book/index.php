<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BookQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'method' => 'GET',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($searchModel, 'author_id')->dropDownList(\app\models\Author::getList(), ['prompt' => 'Автор']) ?>

    <?= $form->field($searchModel, 'name') ?>

    <?= $form->field($searchModel, 'date1', [])->widget(
        \kartik\date\DatePicker::classname(), [
            //'name' => 'check_issue_date',
            'options' => ['placeholder' => 'От'],
            'type' => \kartik\date\DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]
        );?>

    <?= $form->field($searchModel, 'date2')->widget(
        \kartik\date\DatePicker::classname(), [
            //'name' => 'check_issue_date',
            'options' => ['placeholder' => 'До'],
            'type' => \kartik\date\DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]
    );?>


    <div class="form-group">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <?= Html::a('Сбросить', ['index'], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>

    </div>
    <?php \yii\bootstrap\ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'class'     => \yii\grid\DataColumn::className(),
                'attribute' => 'preview',
                'format'    => 'raw',
                'value'     => function ($data) {
                    if(!$data->preview)
                        return '';
                    $preview = '/upload/image/'.$data->preview;
                    $img = Html::img($preview, ['width' => 200]);

                    return Html::a($img, '#', [
                        'class' => 'showModalButton',
                        'data-image' => $preview,
                    ]);

                },
            ],
            [
                'class'     => \yii\grid\DataColumn::className(),
                'label'     => 'Автор',
                'attribute' => 'author.firstname',
                'format'    => 'raw',
                'value'     => function ($data) {
                    return $data->author->firstname.' '.$data->author->lastname;
                },
            ],
            'date',
            [
                'class'     => \yii\grid\DataColumn::className(),
                'label'     => 'Дата создания',
                'attribute' => 'created_at',
                'format'    => 'raw',
                'value'     => function ($data) {
                    return date("Y-m-d H:i:s", $data->created_at);
                },
            ],

            [   'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>','#', [
                            'title' => Yii::t('yii', 'View'),
                            'class' => 'showModalButton',
                            'data-url' => '/book/view/'.$key,
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>','#', [
                            'title' => Yii::t('yii', 'Update'),
                            'class' => 'showModalButton',
                            'data-url' => '/book/update/'.$key,

                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm'=>"Хотите удалить?",
                            'data-pjax'=>'1'
                        ]);
                    }
                ],


            ],
        ],
    ]);  ?>

</div>
