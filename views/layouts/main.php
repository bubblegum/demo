<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<b>Demo</b>App',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
        $items = [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Авторы', 'url' => ['/author/index']],
            ['label' => 'Книги', 'url' => ['/book/index']]
        ];
        if(Yii::$app->user->isGuest){
            $items[] = ['label' => 'Вход', 'url' => ['/site/login']];
            $items[] = ['label' => 'Регистрация', 'url' => ['/site/registration']];
        } else {
            $items[] = ['label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                     'url' => ['/site/logout'],
                     'linkOptions' => ['data-method' => 'post']];
        }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-right">&copy; <b>Demo</b>App <?= date('Y') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader', 'style' => 'min-height: auto !important; padding: 15px; border-bottom: 0px!important; '],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => [
        'keyboard' => true,

    ]
]);
echo "<div id='modalContent'><div style=\"text-align:center\"><img src=\"/images/loading.gif\"></div></div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>
