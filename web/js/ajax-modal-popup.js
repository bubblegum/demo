$(function(){
    $(document).on('click', '.showModalButton', function(){
        if(!$('#modal').data('bs.modal').isShown){
            $('#modal').modal('show');
        }
        if($(this).data('image') != undefined){
            $('#modal').find('#modalContent').html('<img src="'+$(this).data('image')+'" alt="" style="max-width:100%">');
        } else {
            $('#modal').find('#modalContent')
                .load($(this).data('url'));
        }


    });
});