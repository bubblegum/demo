<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_115227_books extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%book}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'preview' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
            'date' => Schema::TYPE_DATETIME. ' NULL DEFAULT NULL',
            'author_id' => Schema::TYPE_INTEGER. ' NOT NULL',
        ], $tableOptions);

        $this->addForeignKey('author_book', '{{%book}}', 'author_id', '{{%author}}', 'id', 'CASCADE', 'CASCADE');

        $db = Yii::$app->db;
        $db->createCommand()->batchInsert('{{%book}}', ['id', 'name', 'created_at', 'updated_at', 'preview', 'date', 'author_id'], [
            [1, 'Дым опиума', 1437135059, 1437135059, '55a8f0d3b0625.jpg', '2015-07-01 00:00:00', 1],
            [2, 'Ферма', 1437135079, 1437135079, '55a8f0e7d1417.jpg', '2015-07-17 00:00:00', 2],
            [3, '1984', 1437135097, 1437135097, '55a8f0f96516e.jpg', '2015-07-02 00:00:00', 2],
            [4, 'Киндер', 1437135135, 1437135135, '55a8f11f0738f.jpg', '2015-07-09 00:00:00', 3],
            [5, 'Пигмей', 1437135157, 1437135157, '55a8f13520624.jpg', '2015-07-15 00:00:00', 4],
            [6, '3d max', 1437135188, 1437135188, '55a8f1540f274.jpeg', '2015-07-08 00:00:00', 5],
            [7, 'История', 1437135213, 1437135706, '55a8f16d0fdb1.jpg', '2015-07-29 00:00:00', 6],
            [8, 'Секретный сад', 1437135240, 1437135240, '55a8f1881ef61.jpg', '2015-07-30 00:00:00', 7],
            [9, 'Дорога', 1437135486, 1437135486, '55a8f27e39712.jpg', '2015-07-22 00:00:00', 8],
            [10, 'Кровавый меридиан', 1437135519, 1437135519, '55a8f29f4ec15.jpg', '2015-07-28 00:00:00', 8],
            [11, 'Кони', 1437135545, 1437135545, '55a8f2b9a69f3.jpg', '2015-07-21 00:00:00', 8],
            [12, 'Соддом', 1437135577, 1437135577, '55a8f2d91869e.jpg', '2015-07-20 00:00:00', 8],
        ])->execute();

    }

    public function down()
    {
        $this->dropForeignKey('author_book', '{{%book}}');
        $this->dropTable('{{%book}}');
    }
}
