<?php

use yii\db\Schema;
use yii\db\Migration;

class m150716_115056_authors extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%author}}', [
            'id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $db = Yii::$app->db;
        $db->createCommand()->batchInsert('{{%author}}', ['id', 'firstname', 'lastname'], [
            [1, 'Клод', 'Фаррер'],
            [2, 'Джордж', 'Оруелл'],
            [3, 'Катя', 'Метелица'],
            [4, 'Чак', 'Паланик'],
            [5, 'Сергей', 'Бондаренко'],
            [6, 'Ирис', 'Групп'],
            [7, 'Франк', 'Бернет'],
            [8, 'Кормак', 'МакКарти'],
        ])->execute();
    }

    public function down()
    {
        $this->dropTable('{{%author}}');
    }
}
